#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#include <ros/ros.h>
#include <ros/package.h>

#include <pugixml.hpp>

// Includes for motion planning - kautham services
#include <kautham/SetQuery.h>
#include <kautham/GetPath.h>
#include <kautham/OpenProblem.h>
#include <kautham/CloseProblem.h>
#include <kautham/AttachObstacle2RobotLink.h>
#include <kautham/DetachObstacle.h>
#include <kautham/SetRobotsConfig.h>
#include <kautham/ObsPos.h>

// Includes for task planning - FF services
#include <ff/Plan.h>

//Data structure to store the scene info
struct knowledge
{
    std::string objName;
    int index;
    std::vector<double> pos;
    std::vector<double> pos1; //DEFINE THE POSITION IN ROOM 1
    std::vector<double> pos2; //DEFINE THE POSITION IN ROOM 2
};



//! Function that wraps the call to the kautham service that opens a problem
bool kauthamOpenProblem( std::string modelFolder, std::string problemFile )
{
    ros::NodeHandle n;
    ros::service::waitForService("/kautham_node/OpenProblem");

    kautham::OpenProblem kthopenproblem_srv;
    kthopenproblem_srv.request.problem = problemFile;
    kthopenproblem_srv.request.dir.resize(1);
    kthopenproblem_srv.request.dir[0] = modelFolder;

    ros::ServiceClient kthopenproblem_client = n.serviceClient<kautham::OpenProblem>("/kautham_node/OpenProblem");
    kthopenproblem_client.call(kthopenproblem_srv);

    if (kthopenproblem_srv.response.response == true) {
        ROS_INFO( "Kautham Problem opened correctly" );
    } else {
        ROS_ERROR( "ERROR Opening Kautham Problem" );
        ROS_ERROR( "models folder: %s", kthopenproblem_srv.request.dir[0].c_str() );
        ROS_ERROR( "problem file: %s", kthopenproblem_srv.request.problem.c_str() );
    }
}


//! Function that wraps the call to the kautham service that closes a problem
void kauthamCloseProblem()
{
    ros::NodeHandle nh;

    //close kautham problem
    ros::service::waitForService("/kautham_node/CloseProblem");
    kautham::CloseProblem kthcloseproblem_srv;
    ros::ServiceClient  kthcloseproblem_client;

    kthcloseproblem_client = nh.serviceClient<kautham::CloseProblem>("/kautham_node/CloseProblem");
    kthcloseproblem_client.call(kthcloseproblem_srv);

    std::cout << "Kautham Problem closed" << std::endl;
}

//! Function that wraps the call to the kautham service that gets the pose of an obstacle
bool kauthamObstaclePos(int indexobs, std::vector<double> &pos )
{
    ros::NodeHandle nh;

    //get obstacle position
    ros::service::waitForService("/kautham_node/GetObstaclePos");
    kautham::ObsPos kthgetobstaclpos_srv;
    ros::ServiceClient  kthgetobstaclpos_client;

    kthgetobstaclpos_srv.request.index = indexobs;

    kthgetobstaclpos_client = nh.serviceClient<kautham::ObsPos>("/kautham_node/GetObstaclePos");
    kthgetobstaclpos_client.call(kthgetobstaclpos_srv);

    if(kthgetobstaclpos_srv.response.getPos.size() != 0)
    {
        std::cout << "Obstacle pos = ( ";
        for(int i=0; i<kthgetobstaclpos_srv.response.getPos.size(); i++)
        {
            pos[i] = kthgetobstaclpos_srv.response.getPos[i];
            std::cout << kthgetobstaclpos_srv.response.getPos[i] << " ";
        }
        std::cout << ")" << std::endl;
        return true;
    }
    else std::cout << "No position returned" << std::endl;
    return false;
}

//! Function that wraps the call to the kautham service that sets a new query to be solved
bool kauthamSetQuery(std::vector<double> init, std::vector<double> goal)
{
	//Define the handle
    ros::NodeHandle nh;

    //Set query
	//Define server and client
    ros::service::waitForService("/kautham_node/SetQuery");
    kautham::SetQuery setquery_srv;
    ros::ServiceClient  setquery_client;
    setquery_client = nh.serviceClient<kautham::SetQuery>("/kautham_node/SetQuery");

	//Load the query request (init and goal)
    setquery_srv.request.init.resize(init.size());
    setquery_srv.request.goal.resize(goal.size());
    for(int i=0; i<init.size();i++)
    {
        setquery_srv.request.init[i] = init[i];
        setquery_srv.request.goal[i] = goal[i];
    }
	//Call the query service
    setquery_client.call(setquery_srv);

	//Listen the response
    if(setquery_srv.response.response==false)
    {
        std::cout<<"Query has not been set\n";
        return false;
    }
}


//! Function that wraps the call to the kautham service that solves a problem (returns path of configurations)
bool kauthamGetPath(std::vector<std::vector<double>>  &solutionpath, int printpath=1)
{
    //Define the handle
    ros::NodeHandle nh;

    //Find path
    //Define server and client
    ros::service::waitForService("/kautham_node/GetPath");
    kautham::GetPath getpath_srv;
    ros::ServiceClient  getpath_client;
    getpath_client = nh.serviceClient<kautham::GetPath>("/kautham_node/GetPath");

    //Call the getpath service
    getpath_client.call(getpath_srv);

    //Listen the response
    if(getpath_srv.response.response.size()==0)
    {
        std::cout<<"No path found\n";
        return false;
    }
    else
    {
        solutionpath.clear();
        solutionpath.resize(getpath_srv.response.response.size());
        for(int i=0; i<getpath_srv.response.response.size(); i++)
        {
          solutionpath[i].resize(getpath_srv.response.response[i].v.size());
          for(int j=0;j<getpath_srv.response.response[i].v.size();j++)
          {
            solutionpath[i][j] = getpath_srv.response.response[i].v[j];
          }
        }

        std::cout<<"Path found";
        if(printpath){
          for(int i=0; i<getpath_srv.response.response.size(); i++)
          {
            //std::cout<<"( ";
            for(int j=0;j<getpath_srv.response.response[i].v.size();j++)
            {
                std::cout<<getpath_srv.response.response[i].v[j]<<" ";
            }
            std::cout<<"\n";
            //std::cout<<")\n";
          }
        }
        return true;
    }
}


//! Function that wraps the call to the kautham service that attaches an object to a given link
bool kauthamAttachObject(int robotnumber, int linknumber, int objectnumber)
{
    ros::NodeHandle nh;

    //attach obstacle to robot link
    ros::service::waitForService("/kautham_node/AttachObstacle2RobotLink");
    kautham::AttachObstacle2RobotLink attachobstacle2robotlink_srv;
    ros::ServiceClient  attachobstacle2robotlink_client;

    attachobstacle2robotlink_client = nh.serviceClient<kautham::AttachObstacle2RobotLink>("/kautham_node/AttachObstacle2RobotLink");
    attachobstacle2robotlink_srv.request.robot = robotnumber;
    attachobstacle2robotlink_srv.request.link = linknumber;
    attachobstacle2robotlink_srv.request.obs = objectnumber;
    attachobstacle2robotlink_client.call(attachobstacle2robotlink_srv);

    std::cout << "Attached object "<< objectnumber << " to link " << linknumber<< " of robot "<< robotnumber<< std::endl;

    return attachobstacle2robotlink_srv.response.response;
}


//! Function that wraps the call to the kautham service that dettaches an attached object
bool kauthamDetachObject(int objectnumber)
{
    ros::NodeHandle nh;

    //detach obstacle to robot link
    ros::service::waitForService("/kautham_node/AttachObstacle2RobotLink");
    kautham::DetachObstacle detachobstacle_srv;
    ros::ServiceClient  detachobstacle_client;

    detachobstacle_client = nh.serviceClient<kautham::DetachObstacle>("/kautham_node/DetachObstacle");
    detachobstacle_srv.request.obs = objectnumber;
    detachobstacle_client.call(detachobstacle_srv);

    std::cout << "Detached object "<< objectnumber << std::endl;

    return detachobstacle_srv.response.response;
}


//! Function that wraps the call to the kautham service that moves the robot
bool kauthamMoveRobot(std::vector<double> controls)
{
    ros::NodeHandle nh;

    //set robot config
	//Define server and client
    ros::service::waitForService("/kautham_node/SetRobotsConfig");
    kautham::SetRobotsConfig setrobotconfig_srv;
    ros::ServiceClient  setrobotconfig_client;

    setrobotconfig_client = nh.serviceClient<kautham::SetRobotsConfig>("/kautham_node/SetRobotsConfig");
    setrobotconfig_srv.request.config.resize(controls.size());
    for(int i=0; i<controls.size();i++)
        setrobotconfig_srv.request.config[i] = controls[i];
    setrobotconfig_client.call(setrobotconfig_srv);

    if (setrobotconfig_srv.response.response == true)
        std::cout << "Robot moved correctly" << std::endl;
    else
        std::cout << "ERROR failed moveRobot" << std::endl;

    return setrobotconfig_srv.response.response;
}

bool writePath(std::stringstream  &xmldocstream, std::vector<std::vector<double>> &path)
{
    //<Conf> 24.862 68.9431 50 0 0 0.707107 0.707107 </Conf>
    for(uint i=0;i<path.size()-1;i++) //the -1 is to skip the last line which is void - to be revised in kautham
    {
        xmldocstream << "<Conf> ";
        for(uint j=0;j<path[i].size();j++)
        {
            xmldocstream << path[i][j] <<" ";
        }
        xmldocstream << "</Conf>"<<std::endl;
    }
    return true;
}



knowledge knowInf(std::string name, int num)
{
    std::vector<double> objApose(7);
    std::vector<double> objBpose(7);
    double xroom=300; //size of rooms
    double yroom=300;
    double xtable=50; //size of table
    double ytable=70;
    double maxx= (xroom-xtable)/2.0;
    double minx=-(xroom-xtable)/2.0;
    double maxy= (yroom-ytable)/2.0;
    double miny=-(yroom-ytable)/2.0;

//num should be either 1 or 2
    knowledge know;
    know.pos.resize(2);
    know.pos1.resize(2);
    know.pos2.resize(2);

    if(name == "OBJA")
    {
        know.index = 1;//always object 1 (corresponds also to kautham index - the rooms are obstacle number 0 -)
        std::cout<<"objA" << std::endl;
        kauthamObstaclePos(know.index,objApose);
        know.objName = name;
        know.pos1[0] =  0.9;
        know.pos1[1] = 0.1;
        know.pos2[0] = (objApose[0]-minx) / (maxx-minx);//0.6;
        know.pos2[1] = (objApose[1]-minx) / (maxx-minx);//0.8;
    }
    else if(name == "OBJB")
    {
        know.index = num;//object 1 if single object, or two otherwise
        std::cout<<"objB" << std::endl;
        kauthamObstaclePos(know.index,objBpose);
        know.objName = name;
        know.pos1[0] = 0.6;
        know.pos1[1] = 0.1;
        know.pos2[0] = (objBpose[0]-minx) / (maxx-minx);//0.9;
        know.pos2[1] = (objBpose[1]-minx) / (maxx-minx);//0.9;
    }
    else if (name == "ROOM1")
    {
        know.objName = name;
        know.pos[0] = 0.31;
        know.pos[1] = 0.31;
    }
    else if (name == "ROOM2")
    {
        know.objName = name;
        know.pos[0] = 0.31;
        know.pos[1] = 0.67;
    }
    else
    {
        std::cout << " Properties of the requested parameter has not been defined " << std::endl;
    }

    return know;
}

int main (int argc, char **argv)
{
    /////////////////////////////////////
    // INITIALIZATION
    //it must be 4 when called with rosrun, and two more will appear wehn called with roslaunch that adds __name and __log
    if(argc<5)
    {
        std::cout << "Num of parameters is not correct\n";
        std::cout << "Should be: rosrun ktmp ktmp-interface kautham_problem_file pddl_domain_file pddl_facts_file numberofobstacles\n";
        std::cout << "(the Kautham problem folder is: demos/OMPL_geo_demos/Table_Rooms_R2/)\n";
        std::cout << "(the PDDL problem folder is: demos/OMPL_geo_demos/Table_Rooms_R2/ff-domains)\n";

        return -1;
    }
    ROS_INFO("Starting Task and Motion Planning Interface Client");

    std::string kauthamproblem = argv[1];
    std::string pddldomainfile = argv[2];
    std::string pddlfactsfile = argv[3];
    int numberofobstacles = atoi(argv[4]);
    std::cout<<"Using kautham problem: "<< kauthamproblem<<std::endl;
    std::cout<<"Using pddl_facts_file: "<< pddlfactsfile<<std::endl;
    std::cout<<"Using "<<numberofobstacles<< " obstacles" <<std::endl;
    if (numberofobstacles!=1 && numberofobstacles!=2){
	    std::cout<<"numberofobstacles should be either 1 or 2"<<std::endl;
	    return -1;
    }

    ros::init(argc, argv, "exercise1_interface_client");
    ros::NodeHandle nh;

    // SETTING PROBLEM FILES OF E1
    std::string kauthamclient_path = ros::package::getPath("exercise1");
    std::string modelFolder = kauthamclient_path + "/demos/models/";
    std::string kauthamProblemFile = kauthamclient_path + "/demos/OMPL_geo_demos/" + kauthamproblem; //PROBLEM FOLDER + PROBLEM FILE
    std::string pddlDomainFile = kauthamclient_path + "/demos/ff-domains/" + pddldomainfile;
    std::string pddlProblemFile = kauthamclient_path + "/demos/ff-domains/" + pddlfactsfile;
    /////////////////////////////////////

    //SOLVING TASK PLANNING PROBLEM
    ros::service::waitForService("/FFPlan");
    ros::ServiceClient ff_client = nh.serviceClient<ff::Plan>("/FFPlan");
    ff::Plan ff_srv;

    //Loading the request for the ff service: the problem file and the domain file
    ff_srv.request.problem = pddlProblemFile;
    ff_srv.request.domain = pddlDomainFile;


    ROS_INFO_STREAM(kauthamProblemFile);
    ROS_INFO_STREAM(pddlDomainFile);
    ROS_INFO_STREAM(pddlProblemFile);

    //Calling the service
    ff_client.call(ff_srv);

    //printing the response
    ROS_INFO("The computed plan is:");
    std::stringstream taskPlan;
    if(ff_srv.response.response)
    {
        for(int i=0; i<ff_srv.response.plan.size(); i++)
        {
            taskPlan << ff_srv.response.plan[i] <<std::endl;
        }
        ROS_INFO_STREAM(taskPlan.str());
    }
    else{
        ROS_INFO("Not able to compute task plan");
	    return -1;
    }

    /////////////////////////////////////
    //SOLVING MOTION PLANNING PROBLEM
    //open kautham problem
    kauthamOpenProblem(modelFolder, kauthamProblemFile);



    // save document to file
    pugi::xml_document *doc = new pugi::xml_document;
    std::stringstream  xmldocstream;
    std::stringstream  xmlfilestream;

    xmldocstream<<"<Task name='"<<kauthamproblem<<"'>"<<std::endl;
    xmlfilestream<<"taskplan_"<<kauthamproblem<<std::endl;

    std::vector<std::vector<double>>  path;

    std::string action;
    std::string rob;
    std::string frLoc;
    std::string toLoc;
    std::string obstacle;

    int obsIndex;
    int robotIndex = 0;
    int linkIndex = 0;

    knowledge initKnow;
    knowledge goalKnow;
    knowledge obsKnow;

    std::vector<double>init;
    std::vector<double>goal;
    std::vector<double>obsPos;



    int inroom = 1;
    bool graspedobject=false;
    std::string line;

    while (std::getline(taskPlan, line))
    {
        std::cout << line << '\n';

        std::istringstream iss(line);
        iss >> action;
        iss >> rob;

        if (action == "MOVE")
        {
            iss >> frLoc;
            iss >> toLoc;
            std::cout << std::endl;
            std::cout << action << " " << rob << " " << frLoc << " " << toLoc << '\n';

            initKnow = knowInf(frLoc, numberofobstacles);
            goalKnow = knowInf(toLoc, numberofobstacles);

            init = initKnow.pos;
            goal = goalKnow.pos;

            std::cout << "Init = " << initKnow.objName << " index = " << initKnow.index << std::endl;
            std::cout << "Goal = " << goalKnow.objName << " index = " << initKnow.index << std::endl;
            std::cout << "Init = (" << init[0] << ", " << init[1] << ")" << std::endl;
            std::cout << "Goal = (" << goal[0] << ", " << goal[1] << ")" << std::endl;

            // Set the Move query to the Kautham by passing init & goal . . .
            kauthamSetQuery(init, goal);
            //solve query
            std::cout << "Solving query" << std::endl;
            if (kauthamGetPath(path))
            {
                if(graspedobject==false)
                {
                    xmldocstream <<"<Transit>"<<std::endl;
                    writePath(xmldocstream, path);
                    xmldocstream <<"</Transit>"<<std::endl;
                }
                else
                    writePath(xmldocstream, path);

                kauthamMoveRobot(goal);
                if (goalKnow.objName == "ROOM1")
                    inroom = 1;
                else
                    inroom = 2;
            }
            else
            {
                std::cout << "Get Path failed. No MOVE possible. Infeasible TASK PLAN" << std::endl;
                break;
            }
        }

        else if (action == "PICK")
        {
            iss >> obstacle;
            iss >> frLoc;
            std::cout << std::endl;
            std::cout << action << " " << rob << " " << obstacle << " " << frLoc << '\n';

            initKnow = knowInf(frLoc, numberofobstacles);
            obsKnow = knowInf(obstacle, numberofobstacles);
            obsIndex = obsKnow.index;
            obsPos = obsKnow.pos;

            if (inroom == 1)
            {
                init = initKnow.pos; //from center of room
                goal = obsKnow.pos1; //to where object is
            }
            else
            {
                init = initKnow.pos;
                goal = obsKnow.pos2;
            }

            // Set the Move query to the Kautham by passing init & goal . . .

            std::cout << "Moving to object position " << std::endl;
            std::cout << "Init = (" << init[0] << ", " << init[1] << ")" << std::endl;
            std::cout << "Goal = (" << goal[0] << ", " << goal[1] << ")" << std::endl;
            kauthamSetQuery(init, goal);
            //solve query
            std::cout << "Solving query to pick object" << std::endl;
            if (kauthamGetPath(path))
            {
                xmldocstream <<"<Transit>"<<std::endl;
                writePath(xmldocstream, path);
                xmldocstream <<"</Transit>"<<std::endl;

                kauthamMoveRobot(goal);
            }
            else
            {
                std::cout << "Get Path failed. No MOVE TO PICK possible. Infeasible TASK PLAN" << std::endl;
                break;
            }

            // Set the Pick query to the Kautham . . .
            std::cout << "Picking object " << obsIndex << std::endl;
            kauthamAttachObject(robotIndex, linkIndex, obsIndex);

            //come back to center of room with the picked object
            std::cout << "Moving to center of room " << std::endl;
            std::cout << "Init = (" << goal[0] << ", " << goal[1] << ")" << std::endl;
            std::cout << "Goal = (" << init[0] << ", " << init[1] << ")" << std::endl;
            kauthamSetQuery(goal, init);
            if (kauthamGetPath(path))
            {
                //Start transfer
                graspedobject = true;
	            //<Transfer object="1" robot="0" link="0">
                xmldocstream <<"<Transfer object='"<< obsIndex << "' robot='0' link='0' >"<<std::endl;
                writePath(xmldocstream, path);

                kauthamMoveRobot(init);
            }
            else
            {
                std::cout << "Get Path failed. No MOVE TO after PICK possible. Infeasible TASK PLAN" << std::endl;
                break;
            }
        }

        else if (action == "PLACE")
        {
            iss >> obstacle;
            iss >> toLoc;
            std::cout << std::endl;
            std::cout << action << " " << rob << " " << obstacle << " " << toLoc << '\n';

            obsKnow = knowInf(obstacle, numberofobstacles);
            goalKnow = knowInf(toLoc, numberofobstacles);

            obsIndex = obsKnow.index;

            if (inroom == 1)
            {
                init = goalKnow.pos; //from center of room
                goal = obsKnow.pos1; //to where object mut be placed
            }
            else
            {
                init = goalKnow.pos;
                goal = obsKnow.pos2;
            }

            // Set the Move query to the Kautham by passing init & goal . . .
            std::cout << "Moving to where object should be placed " << std::endl;
            std::cout << "Init = (" << init[0] << ", " << init[1] << ")" << std::endl;
            std::cout << "Goal = (" << goal[0] << ", " << goal[1] << ")" << std::endl;
            kauthamSetQuery(init, goal);
            //solve query
            std::cout << "Solving query to pick object" << std::endl;
            if (kauthamGetPath(path))
            {
                //finish transfer
                writePath(xmldocstream, path);
                xmldocstream <<"</Transfer>"<<std::endl;
                graspedobject = false;

                kauthamMoveRobot(goal);
            }
            else
            {
                std::cout << "Get Path failed. No MOVE TO PLACE possible. Infeasible TASK PLAN" << std::endl;
                break;
            }

            // Set the Place query to the Kautham . . .
            std::cout << "Placing object " << obsIndex << std::endl;
            kauthamDetachObject(obsIndex);

            //come back to center of room without the object
            std::cout << "Moving to center of room " << std::endl;
            std::cout << "Init = (" << goal[0] << ", " << goal[1] << ")" << std::endl;
            std::cout << "Goal = (" << init[0] << ", " << init[1] << ")" << std::endl;
            kauthamSetQuery(goal, init);
            if (kauthamGetPath(path))
            {
                kauthamMoveRobot(init);
                xmldocstream <<"<Transit>"<<std::endl;
                writePath(xmldocstream, path);
                xmldocstream <<"</Transit>"<<std::endl;
            }
            else
            {
                std::cout << "Get Path failed. No MOVE TO after PLACE possible. Infeasible TASK PLAN" << std::endl;
                break;
            }
        }

        else
        {
            std::cout << "Action has not been defined " << '\n';
        }
    }

    //close kauthm problem
    std::cout << std::endl;
    kauthamCloseProblem();

    //close and save the xml document
    xmldocstream <<"</Task>";
    doc->load(xmldocstream);

    std::string tfile = kauthamclient_path + "/demos/OMPL_geo_demos/Table_Rooms_R2/taskfile.xml";
    if(doc->save_file(tfile.c_str()))
    {
        std::cout << "SAVING RESULT TO " << tfile<< std::endl;
    }
    else
    {
        std::cout << "FAILED TO SAVE RESULT TO " << tfile << std::endl;
    }
    return 0;
}
